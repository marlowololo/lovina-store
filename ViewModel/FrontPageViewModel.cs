using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace lovina_store.Data.ViewModels
{
    public class FrontPageViewModel
    {
        public FrontPageViewModel()
        {
        }
        
        public IEnumerable<Product> PrimeProducts { get; set; }
        public Contact Contact{ get; set; }
    }
}