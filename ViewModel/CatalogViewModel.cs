using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace lovina_store.Data.ViewModels
{
    public class CatalogViewModel
    {
        public CatalogViewModel()
        {
        }
        
        public IEnumerable<Product> Products{ get; set; }
        public IEnumerable<Category> Categories{ get; set; }
    }
}