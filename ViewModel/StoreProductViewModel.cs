using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace lovina_store.Data.ViewModels
{
    public class StoreProductViewModel
    {
        public StoreProductViewModel()
        {
        }
        
        public Product Product{ get; set; }
        public Contact Contact{ get; set; }
    }
}