using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace lovina_store.Data.ViewModels
{
    public class CreateProductViewModel
    {
        public CreateProductViewModel()
        {
        }
        
        public Product Product { get; set; }
        public IEnumerable<Category> Categories{ get; set; }
    }
}