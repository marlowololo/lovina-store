using System.Linq;
using lovina_store.Data.Interfaces;
using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace lovina_store.Data.Repository
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(StoreDbContext context) : base(context)
        {
        }
    }
}