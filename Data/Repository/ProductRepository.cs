using System;
using System.Collections.Generic;
using System.Linq;
using lovina_store.Data.Interfaces;
using lovina_store.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace lovina_store.Data.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(StoreDbContext context) : base(context)
        {
        }

        public IEnumerable<Product> GetAllWithCategory()
        {
            return _context.Products.Include(c => c.Category);
        }

        public IEnumerable<Product> FindWithCategory(Func<Product,bool> predicate)
        {
            return _context.Products.Include(c => c.Category).Where(predicate);
        }

        public IEnumerable<Product> GetAllPrimeProduct()
        {
            return Find(p => p.IsPrime);
        }
    }
}