using System.Linq;
using lovina_store.Data.Interfaces;
using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace lovina_store.Data.Repository
{
    public class ContactRepository : Repository<Contact>, IContactRepository
    {
        public ContactRepository(StoreDbContext context) : base(context)
        {
        }
    }
}