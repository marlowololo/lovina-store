using lovina_store.Data.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lovina_store.Data
{
    public static class DbInitializer
    {
        public static void Seed(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<StoreDbContext>();
                if(context.Database.EnsureCreated()){
                    Contact contact = new Contact(){
                        ID=1,
                        Handphone="-",
                    };
                    context.Contacts.Add(contact);
                    context.SaveChanges();
                }
                
                // Add Category

                // var Bedak = new Category { Name = "Bedak" };
                
                // context.Categories.Add(Bedak);

                // // Add Product
                // var justin = new Product { Name = "A", Category = Bedak};

                // var willie = new Product { Name = "B", Category = Bedak};

                // var leoma = new Product { Name = "C", Category = Bedak};

                // context.Products.Add(justin);
                // context.Products.Add(willie);
                // context.Products.Add(leoma);

            }
        }
    }
}