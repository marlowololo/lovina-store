using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace lovina_store.Data.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}