using System;
using System.Collections.Generic;
using lovina_store.Data.Models;

namespace lovina_store.Data.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetAllWithCategory();
        IEnumerable<Product> FindWithCategory(Func<Product,bool> predicate);
    }
}