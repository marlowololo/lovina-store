using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lovina_store.Data.Models
{
    public class Product
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Nama produk tidak valid, pastikan mengisi nama produk sesuai ketentuan dan tidak boleh kosong.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Deskripsi tidak valid, pastikan mengisi deskripsi produk sesuai ketentuan dan tidak boleh kosong.")]
        public string Description { get; set; }

        public int Price { get; set; }

        public bool StockReady { get; set; }

        public bool IsPrime { get; set; }

        public byte Discount { get; set; }

        [Required(ErrorMessage = "Pastikan pilih kategori!")]
        public int CategoryID {get; set;}
        
        public virtual Category Category { get; set; }

        public int DiscountedPrice { get { return Price - (Price * Discount / 100); } }
    }
}