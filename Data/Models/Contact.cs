using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lovina_store.Data.Models
{
    public class Contact
    {
        public int ID { get; set; }
        public string Handphone { get; set; }
        public string Instagram { get; set; }
        public string InstagramLink { get; set; }
        public string Facebook { get; set; }
        public string FacebookLink { get; set; }
        public string WA { get; set; }
        public string Line { get; set; }
    }
}