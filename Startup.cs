﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using lovina_store.Data;
using lovina_store.Data.Interfaces;
using lovina_store.Data.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace lovina_store
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IContactRepository, ContactRepository>();
            services.AddDbContext<StoreDbContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection"))
            );
            services.AddSession(options =>
                options.IdleTimeout = TimeSpan.FromMinutes(30)
            );
            services.AddMvc(options => {
                options.ModelBindingMessageProvider.SetAttemptedValueIsInvalidAccessor((x, y) => $"Data tidak valid. Format '{x}' tidak didukung sistem, coba masukan data sesuai petunjuk.");
                options.ModelBindingMessageProvider.SetValueMustNotBeNullAccessor((x) => $"Data tidak boleh kosong");
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles(new StaticFileOptions
            {
                ServeUnknownFileTypes = true
            });
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Store}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "AdminProduct",
                    template: "Admin/Product/{action}/{id?}"
                );
                routes.MapRoute(
                    name: "AdminCategory",
                    template: "Admin/Category/{action}/{id?}"
                );
                routes.MapRoute(
                    name: "AdminContact",
                    template: "Admin/Contact/"
                );

            });
            
            DbInitializer.Seed(app);
        }
    }
}
