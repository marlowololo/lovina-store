var productImageOffset = 250;
var winHeight = 0;
var navbarHeight = 80;
var winWidth = $(window).width();

$(document).ready(function(){
    $('.golosor').css("top", 10);
    winHeight = $(window).height();
    if(winWidth<=475){
        $("#search-bar").css("display","none");
    }
});

$( window ).scroll(function() {
    var scroll = $(window).scrollTop();
    var productScroll = productImageOffset-(scroll/2);
    $( ".golosor" ).css("top", productScroll>-85?productScroll:-85);
    if(winWidth<=475){
        if(scroll<=navbarHeight){
            $("#search-bar").css("display","none");
        } else {
            $("#search-bar").css("display","block");
        }
    }
});