var navbarHeight = 80;

$(document).ready(function(){
    $("#navbar-fixed").css("background-color", "rgba(0,0,0,"+0+")");
    $("input#search-bar").css("color","#fff");
    var winWidth = $(window).width();
    if(winWidth<=475){
        $("#logo").css("float","none");
        $("#logo").css("margin","0px auto");
        $("#links").css("float","none");
        $("#links").css("margin","0px 10px");
        $("#navbar-fixed").css("height","160px");
    }
});

$( window ).scroll(function() {
    var scroll = $(window).scrollTop();
    if(scroll>=navbarHeight){
        $("#navbar-fixed").css("background-color", "rgba(255,255,255,"+1+")");
        $("input#search-bar").css("color","#000");
        $("#search-logo").css("color","#f48fb1")
    } else {
        $("#navbar-fixed").css("background-color", "rgba(0,0,0,"+0+")");
        $("input#search-bar").css("color","#fff");
        $("#search-logo").css("color","#fff")
    }
});