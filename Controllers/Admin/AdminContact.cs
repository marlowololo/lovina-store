using lovina_store.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using lovina_store.Data.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using lovina_store.Data.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.FileProviders;
using System.Collections.Generic;

namespace lovina_store.Controllers
{
    public class AdminContactController : Controller
    {

        private readonly IContactRepository _contactRepository;

        public AdminContactController(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        [Route("Admin/Contact")]
        [Route("Admin/Contact/Index")]
        public IActionResult Index()
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            return View(_contactRepository.GetById(1));
        }

        [HttpPost]
        [Route("Admin/Contact/Index/")]
        public IActionResult Index(Contact contact)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            if(ModelState.IsValid){
                _contactRepository.Update(contact);
                return RedirectToAction("Success");
            }
            return View(contact);
        }

        [Route("Admin/Contact/Success")]
        public IActionResult Success()
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            return View();
        }

    }
}
