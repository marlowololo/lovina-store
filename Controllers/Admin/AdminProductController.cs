using lovina_store.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using lovina_store.Data.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using lovina_store.Data.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.FileProviders;
using System.Collections.Generic;
using System;

namespace lovina_store.Controllers
{
    public class AdminProductController : Controller
    {

        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;

        public AdminProductController(IProductRepository productRepository, ICategoryRepository categoryRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
        }

        [Route("Admin/Product")]
        [Route("Admin/Product/Index")]
        public IActionResult Index(string keyword)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }
            if(keyword!=null){
                return View(_productRepository.FindWithCategory(p => p.Name.Contains(keyword, StringComparison.CurrentCultureIgnoreCase)));
            } else {
                return View(_productRepository.GetAllWithCategory());
            }
        }

        [Route("Admin/Product/Create")]
        public IActionResult Create()
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }
            CreateProductViewModel productVM = new CreateProductViewModel();

            productVM.Categories = _categoryRepository.GetAll();
            productVM.Product = new Product();
            return View(productVM);
        }

        [HttpPost]
        [Route("Admin/Product/Create")]
        public IActionResult Create(CreateProductViewModel productVM, IFormFile productPhotos)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            if (productPhotos == null || productPhotos.Length == 0)
                return Content("File gambar/foto produk tidak terdeteksi");
                //OR SHOULD WE USE DEFAULT IMAGE?
            
            if (productPhotos.ContentType != "image/jpeg" && productPhotos.ContentType != "image/png")
                return Content("Hanya dapat menerima gambar berupa jpg atau png");
                
            if(ModelState.IsValid){
                _productRepository.Create(productVM.Product);

                var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot/product-photos/", 
                productVM.Product.ID.ToString());

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    productPhotos.CopyTo(stream);
                }

                return RedirectToRoute("AdminProduct",
                new { action = "Index" });
            }
            productVM.Categories = _categoryRepository.GetAll();
            return View(productVM);
        }

        [Route("Admin/Product/Edit/{productId}")]
        public IActionResult Edit(int productId)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            Product product = _productRepository.GetById(productId);

            if(product==null){
                return NotFound();
            }

            CreateProductViewModel productVM = new CreateProductViewModel();

            productVM.Product = product;
            productVM.Categories = _categoryRepository.GetAll();
            return View(productVM);
        }

        [HttpPost]
        [Route("Admin/Product/Edit/{productId}")]
        public IActionResult Edit(CreateProductViewModel productVM, IFormFile productPhotos)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            bool updatePhoto = false;
            if (productPhotos != null){
                if(productPhotos.Length != 0) {
                    updatePhoto = true;
                    //OR SHOULD WE USE DEFAULT IMAGE?
                    if (productPhotos.ContentType != "image/jpeg" && productPhotos.ContentType != "image/png")
                        return Content("Hanya dapat menerima gambar berupa jpg atau png");
                }
            }

            productVM.Product.Category = _categoryRepository.Find(x => x.ID == productVM.Product.CategoryID).ElementAt(0);
            if(ModelState.IsValid){
                _productRepository.Update(productVM.Product);

                if(updatePhoto){
                    var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot/product-photos/", 
                    productVM.Product.ID.ToString());

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        productPhotos.CopyTo(stream);
                    }
                }

                return RedirectToRoute("AdminProduct",
                new { action = "Index" });
            }
            productVM.Categories = _categoryRepository.GetAll();
            return View(productVM);
        }

        [Route("Admin/Product/Delete/{productId}")]
        public IActionResult Delete(int productId)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            Product product = _productRepository.GetById(productId);

            if(product==null){
                return NotFound();
            }

            _productRepository.Delete(product);

            return RedirectToRoute("AdminProduct",
            new { action = "Index" });
        }

    }
}
