using System.Diagnostics;
using lovina_store.Data;
using lovina_store.Data.Interfaces;
using lovina_store.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace lovina_store.Controllers
{
    public class AdminController : Controller
    {

        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;

        public AdminController(IProductRepository productRepository, ICategoryRepository categoryRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Index()
        {
            if(HttpContext.Session.GetInt32("LoggedIn")==1){
                return View();
            } else {
                return RedirectToAction("Login");
            }
        }

        public IActionResult Login(){
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            if(username == "root" && password == "root"){
                HttpContext.Session.SetInt32("LoggedIn", 1);
                return RedirectToAction("Index");
            } else {
                return View();
            }
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("LoggedIn");
            return RedirectToRoute("default",
            new { controller = "Store", action = "Index" });
        }

    }
}
