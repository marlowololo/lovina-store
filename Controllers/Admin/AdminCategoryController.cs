using lovina_store.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using lovina_store.Data.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using lovina_store.Data.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.FileProviders;
using System.Collections.Generic;

namespace lovina_store.Controllers
{
    public class AdminCategoryController : Controller
    {

        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;

        public AdminCategoryController(IProductRepository productRepository, ICategoryRepository categoryRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
        }

        [Route("Admin/Category")]
        [Route("Admin/Category/Index")]
        public IActionResult Index()
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }
            return View(_categoryRepository.GetAll());
        }

        [Route("Admin/Category/Create")]
        public IActionResult Create()
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }
            return View();
        }

        [HttpPost]
        [Route("Admin/Category/Create")]
        public IActionResult Create(Category category)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            if(ModelState.IsValid){
                _categoryRepository.Create(category);
                return RedirectToRoute("AdminCategory",
                new { action = "Index" });
            }

            return View(category);
        }

        [Route("Admin/Category/Edit/{categoryId}")]
        public IActionResult Edit(int categoryId)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            Category category = _categoryRepository.GetById(categoryId);

            if(category==null){
                return NotFound();
            }

            return View(category);
        }

        [HttpPost]
        [Route("Admin/Category/Edit/{categoryId}")]
        public IActionResult Edit(Category category)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }    
            
            if(ModelState.IsValid){
                _categoryRepository.Update(category);
                return RedirectToRoute("AdminCategory",
                new { action = "Index" });
            }
            return View(category);
        }

        [Route("Admin/Category/Delete/{categoryId}")]
        public IActionResult Delete(int categoryId)
        {
            if(HttpContext.Session.GetInt32("LoggedIn")!=1){
                return RedirectToRoute("default",
                new { controller = "Admin", action = "Login" });
            }

            Category category = _categoryRepository.GetById(categoryId);

            if(category==null){
                return NotFound();
            }

            _categoryRepository.Delete(category);

            return RedirectToRoute("AdminCategory",
            new { action = "Index" });
        }

    }
}
