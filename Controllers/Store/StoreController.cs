﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using lovina_store.Data;
using lovina_store.Data.Interfaces;
using lovina_store.Data.Models;
using lovina_store.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace lovina_store.Controllers
{
    public class StoreController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly IContactRepository _contactRepository;
        private readonly ICategoryRepository _categoryRepository;

        public StoreController(IProductRepository productRepository, IContactRepository contactRepository, ICategoryRepository categoryRepository)
        {
            _productRepository = productRepository;
            _contactRepository = contactRepository;
            _categoryRepository = categoryRepository;
        }

        public IActionResult Index()
        {
            FrontPageViewModel frontPageVM = new FrontPageViewModel();

            frontPageVM.PrimeProducts = _productRepository.FindWithCategory(p => p.IsPrime);
            frontPageVM.Contact = _contactRepository.GetById(1);
            return View(frontPageVM);
        }

        public IActionResult Catalog(int? categoryID, string keyword)
        {
            CatalogViewModel catalogVM = new CatalogViewModel();
            
            IEnumerable<Product> products = _productRepository.GetAll();
            if(categoryID!=null){
                products = products.Where(p => p.CategoryID == categoryID);
            }
            if(keyword!=null){
                products = products.Where(p => p.Name.Contains(keyword));
            }
            catalogVM.Products = products;

            catalogVM.Categories = _categoryRepository.GetAll();
            return View(catalogVM);
        }

        public IActionResult Product(int? id){

            if(id==null){
                return NotFound();
            }

            StoreProductViewModel storeProductVM = new StoreProductViewModel();

            //storeProductVM.Product = 
            storeProductVM.Product = _productRepository.GetById((int)id);

            if(storeProductVM.Product==null){
                return NotFound();
            }

            storeProductVM.Contact = _contactRepository.GetById(1);

            return View(storeProductVM);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
